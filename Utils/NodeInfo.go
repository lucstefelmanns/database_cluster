package Utils

type NodeInfo struct {
	Addr			string
	Port			uint32
	BaseHashGroup	uint32
	IsLocal			bool
}
